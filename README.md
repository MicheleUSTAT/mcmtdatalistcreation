Il pacchetto "MCMTdlcreation" é stato realizzato per poter ricreare l'oggetto "MCMTdatapkg::Data_list" che contiene tutte le banche dati del MCMT (incluse le variabili necessarie per replicare la tabella A2 dell'UST). Questo oggetto é utilizzato come argomento di "default" nel pacchetto "MCMTdataanalysis" creando una dipendenza tra i pacchetti. Per poter utilizzare i pahchetti di analisi del MCMZ é quindi necesario anche il pachetto "MCMTdatapkg": questo pacchetto contiene i dati forniti dall'UST e quindi non può essere condiviso. Per questo motivo é stato creato il pacchetto "MCMTdlcreation" che partendo dalle banche dati del MCMT permette di ricreare l'oggetto "MCMTdatapkg:Data_list".

Pacchetto con funzioni per ricreare tutti i valori necessari per la creazione delle tabelle e dei grafici così come presenti nel Rapporto dell'UST "Comportement de la population en matière de transports 2015", dalla sezione 2.1 alla 3.6.
Questo pacchetto srutta le funzioni del pacchetto "MCMTtools" che comprende quattro pacchetti:

MCMTdataanalysis
MCMTdataprep
MCMTdlcreation
MCMTdatapkg

Le funzioni interne ai primi due pacchetti necessitano come argometo "data" l'oggetto "MCMTdatapkg::Data_list": questo file di dati é composto dalle banche dati del MCMT includendo però solamente una parte delle variabili (quelle necessarie per ricreare la tabella A2 fornita dall'UST).
Questo oggetto di dati é utilizzato come argomento di "default" per le funzioni di analisi del pacchetto "MCMTdataanalysis" creando una dipendenza tra i pacchetti.
Perhcé le funzioni possano essere utilizzate é dunque necessario disporre anche del pacchetto "MCMTdatapkg": visto che questo pacchetto contiene i dati forniti dall'UST, non può essere fornito gratuitamente e non può essere condiviso in questo progetto. Le stesse osservazioni si applicano al pacchetto "MCMTtools" che contiene i primi tre pacchetti escluso il pacchetto "MCMTdatapkg".
